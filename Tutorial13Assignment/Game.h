#ifndef GAME_H
#define GAME_H

#include <future>
#include "SpriteBatch.h"
#include "SpriteFont.h"

#include "Mesh.h"
#include "Model.h"
#include "singleton.h"
#include "RandomNumberGenerator.h"

class Game : public Singleton<Game>
{
public:
	Game() {}
	~Game() {
		Release();
	}
	void Update(float dTime);
	void Render(float dTime);
	void Initialise();
	void Release();
	LRESULT WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	const DirectX::SimpleMath::Vector3 mDefCamPos = DirectX::SimpleMath::Vector3(0, 15, -5);
	DirectX::SimpleMath::Vector3 mCamPos = mDefCamPos;

	std::vector<Model> mModels;
	std::vector<Model> mBricks;

	Model Ball;
	Model Player;
	enum Modelid {FLOOR, BRICK, TOTAL=2};

private:

	RandonNumberGenerator RNG;

	//data to manage 2nd thread loading
	struct LoadData
	{
		std::future<void> loader;
		int totalToLoad = 0;
		int loadedSoFar = 0;
		bool running = false;
	};
	LoadData mLoadData;
	/// <summary>
	/// Public variables temp move to classes later.
	/// </summary>
	float gAngle = 0;
	DirectX::SimpleMath::Vector3 DefaultPos = DirectX::SimpleMath::Vector3(0, 1, -5);
	DirectX::SimpleMath::Vector3 PlayerPos = DefaultPos;
	bool LaunchBall = false;
	float MoveSpeed_Z = 0.001f;
	/// <summary>
	/// 
	/// </summary>
	DirectX::SpriteBatch *pFontBatch = nullptr;
	DirectX::SpriteFont *pFont = nullptr;

	void Load(); 
	void RenderLoad(float dTime);
};


#endif