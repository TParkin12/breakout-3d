#include "WindowUtils.h"
#include "D3D.h"
#include "Game.h"
#include "GeometryBuilder.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

void Setup(Model& m, Mesh& source, const Vector3& scale, const Vector3& pos, const Vector3& rot)
{
	m.Initialise(source);
	m.GetScale() = scale;
	m.GetPosition() = pos;
	m.GetRotation() = rot;
}

void Setup(Model& m, Mesh& source, float scale, const Vector3& pos, const Vector3& rot)
{
	Setup(m, source, Vector3(scale, scale, scale), pos, rot);
}

void Game::Load()
{
	MyD3D& d3d = WinUtil::Get().GetD3D();

	Model m;
	mModels.insert(mModels.begin(), Modelid::TOTAL, m);


	/// Mesh Initalistation
	///
	/// 
	Mesh& quadMesh		= BuildQuad(d3d.GetMeshMgr());
	Mesh& cubeMesh		= BuildCube(d3d.GetMeshMgr());
	Mesh& sphereMesh	= BuildSphere(d3d.GetMeshMgr(), 10 , 10 );
	///
	/// 
	/// 

	///textured lit paddle/bat Intialisation 
	/// 
	/// 
	Setup(Player, cubeMesh, Vector3(1.5f, 0.1f, 0.2f), DefaultPos, Vector3(0, 0, 0));
	Material mat = Player.GetMesh().GetSubMesh(0).material;
	mat.gfxData.Set(Vector4(0.5, 0.5, 0.5, 1), Vector4(1, 1, 1, 0), Vector4(1, 1, 1, 1));
	mat.pTextureRV = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "Building1.dds");
	mat.flags |= Material::TFlags::ALPHA_TRANSPARENCY;
	mat.SetBlendFactors(0, 0, 0, 1);
	Player.SetOverrideMat(&mat);
	mLoadData.loadedSoFar++;
	///
	/// 
	/// 
	
	///BALL Intialisation
	///  
	/// 
	Setup(Ball, sphereMesh, Vector3(0.5f, 0.5f, 0.5f), Vector3(0, 1, -4), Vector3(0, 0, 0));
	mat = Ball.GetMesh().GetSubMesh(0).material;
	mat.gfxData.Set(Vector4(0.5, 0.5, 0.5, 1), Vector4(1, 1, 1, 0), Vector4(1, 1, 1, 1));
	mat.pTextureRV = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "Building2.dds");
	mat.flags |= Material::TFlags::ALPHA_TRANSPARENCY;
	mat.SetBlendFactors(0, 0, 0, 1);
	Ball.SetOverrideMat(&mat);
	mLoadData.loadedSoFar++;
	///
	/// 
	/// 
	/// 
	
	/// Bricks Intialisation 
	/// 
	///
	Setup(mModels[Modelid::BRICK], cubeMesh, Vector3(1.5f, 0.5f, 0.5f), Vector3(-1, 0, 5), Vector3(0, 0, 0));
	int x = -5, z = 5;
	mBricks.reserve(9);
	for (int i = 0; i < 9; i++)
	{
			mModels[Modelid::BRICK].GetScale() = Vector3(1.6f, 0.5f, 0.8f);
			mModels[Modelid::BRICK].GetPosition() = Vector3(x, 1, z);
			mat.gfxData.Set(Vector4(0.5, 0.5, 0.5, 1), Vector4(1, 1, 1, 0), Vector4(1, 1, 1, 1));
			mat.pTextureRV = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "Building2.dds");
			mat.flags |= Material::TFlags::ALPHA_TRANSPARENCY;
			mat.SetBlendFactors(0, 0, 0, 1);
			mModels[Modelid::BRICK].SetOverrideMat(&mat);
			mBricks.push_back(mModels[Modelid::BRICK]);
			x += 5;


			if (i % 3 == 2)
			{
				z -= 1.5;
				x = -5;
			}
			 
			mLoadData.loadedSoFar++;
	}
	//-------------------------------------------------------------------------------------------------------------------



	///Ground Quad Intialisation 
	/// 
	/// 
	Setup(mModels[Modelid::FLOOR], quadMesh, Vector3(7, 1, 7), Vector3(0, 0, 0), Vector3(0, 0, 0));	
	mat = mModels[Modelid::FLOOR].GetMesh().GetSubMesh(0).material;
	mat.gfxData.Set(Vector4(0.9f, 0.8f, 0.8f, 0), Vector4(0.9f, 0.8f, 0.8f, 0), Vector4(0.9f, 0.8f, 0.8f, 1));
	mat.pTextureRV = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "ground.dds");
	mat.texture = "ground.dds";
	mModels[Modelid::FLOOR].SetOverrideMat(&mat);
	mLoadData.loadedSoFar++;
	///
	/// 
	/// 
	
	///Global light Intialisation
	///  
	/// 
	d3d.GetFX().SetupDirectionalLight(0, true, Vector3(-0.5f, -0.5f, 0.5f), Vector3(0.47f, 0.47f, 0.47f), Vector3(0.3f, 0.3f, 0.3f), Vector3(0.25f, 0.25f, 0.25f));
	///
	/// 
	/// 
}

void Game::Initialise()
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	pFontBatch = new SpriteBatch(&d3d.GetDeviceCtx());
	assert(pFontBatch);
	pFont = new SpriteFont(&d3d.GetDevice(), L"../bin/data/fonts/algerian.spritefont");
	assert(pFont);
	   
	mLoadData.totalToLoad = 11;
	mLoadData.loadedSoFar = 0;
	mLoadData.running = true;
	mLoadData.loader = std::async(launch::async, &Game::Load, this);
}

/// <summary>
///  Releases all assets
/// </summary>
void Game::Release()
{
	delete pFontBatch;
	pFontBatch = nullptr;
	delete pFont;
	pFont = nullptr;
}

void Game::Update(float dTime)
{
	gAngle += dTime * 0.5f;
}

void Game::Render(float dTime)
{
	if (mLoadData.running)
	{
		if (!mLoadData.loader._Is_ready())
		{
			RenderLoad(dTime);
			return;
		}
		mLoadData.loader.get();
		mLoadData.running = false;
		return;
	}

	MyD3D& d3d = WinUtil::Get().GetD3D();
	d3d.BeginRender(Colours::Black); //default background colour

	float alpha = 0.5f + sinf(gAngle * 2)*0.5f;

	/// 
	/// Camera Code
	/// 
	d3d.GetFX().SetPerFrameConsts(d3d.GetDeviceCtx(), mCamPos);

	CreateViewMatrix(d3d.GetFX().GetViewMatrix(), mCamPos, Vector3(0, 0, 0), Vector3(0, 1, 0));
	CreateProjectionMatrix(d3d.GetFX().GetProjectionMatrix(), 0.25f*PI, WinUtil::Get().GetAspectRatio(), 1, 1000.f);
	Matrix w = Matrix::CreateRotationY(sinf(gAngle));
	d3d.GetFX().SetPerObjConsts(d3d.GetDeviceCtx(), w);
	
	/// 
	/// Ball Code
	/// 
	Vector3 Direction = Vector3(0, 1, 0);
	Matrix m = Matrix::CreateRotationY(Ball.GetRotation().y);
	Direction = Vector3::TransformNormal(Direction, m);
	d3d.GetFX().SetupSpotLight(1, true, Vector3(Ball.GetPosition().x , Ball.GetPosition().y - 1, Ball.GetPosition().z), Direction, Vector3(0.2f, 0.05f, 0.05f), Vector3(0.01f, 5.f, 0.01f), Vector3(0.01f, 0.01f, 0.01f));
	Direction *= -1;


	if (LaunchBall)
	{
		
		

		if (Ball.GetPosition().z == 6)
			MoveSpeed_Z = -MoveSpeed_Z;

		bool Intersect(Vector3, Vector3);
		if (Intersect(Player.GetPosition(), Ball.GetPosition()))
			MoveSpeed_Z = 0.001f;

		Ball.GetPosition().z += MoveSpeed_Z;

		Ball.GetPosition().z = std::clamp(Ball.GetPosition().z, -6.f, 6.f);
	}
	else
		Ball.GetPosition().x = Player.GetPosition().x;

	d3d.GetFX().Render(Ball);
	//-------------------------------------------------------------------------------------------------------------------

	/// 
	/// Rendering of Vector of static objects i.e (background/floor/walls)
	/// 
	for (auto& mod : mModels)
	{
		d3d.GetFX().Render(mod);
	}
	//-------------------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------------------
	/// 
	/// Player Code
	/// 
	Player.GetPosition() = PlayerPos;
	d3d.GetFX().Render(Player);
	//-------------------------------------------------------------------------------------------------------------------

	/// 
	/// Rendering of Vector of bricks
	/// 
	for (auto& mod : mBricks)
		d3d.GetFX().Render(mod);
	//-------------------------------------------------------------------------------------------------------------------
	/// 
	/// Rendering of text
	///
	pFontBatch->Begin();
	wstringstream ss;
	//ss << mCamPos.x << ' ' << mCamPos.y << ' ' << mCamPos.z;
	ss << Ball.GetPosition().z;
	pFont->DrawString(pFontBatch, ss.str().c_str(), Vector2(0, 0), Colours::White, 0, Vector2(0, 0), Vector2(1.f, 1.f));
	pFontBatch->End();
	//-------------------------------------------------------------------------------------------------------------------
	d3d.EndRender();
}


bool Intersect(Vector3 Object1, Vector3 Object2)
{
	Vector3 tempBox;

	tempBox.x = (std::max)(Object1.x, (std::min)(Object2.x, Object1.x));
	tempBox.y = (std::max)(Object1.y, (std::min)(Object2.y, Object1.y));
	tempBox.z = (std::max)(Object1.z, (std::min)(Object2.z, Object1.z));

	float distance = std::sqrt((tempBox.x - Object2.x) * (tempBox.x - Object2.x) + (tempBox.y - Object2.y) * (tempBox.y - Object2.y) + (tempBox.z - Object2.z) * (tempBox.z - Object2.z));

	return distance < 1;
}

/// <summary>
/// Renders the loading screen while assets are being intialised
/// </summary>
void Game::RenderLoad(float dTime)
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	d3d.BeginRender(Colours::Black);

	pFontBatch->Begin();

	static int pips = 0;
	static float elapsed = 0;
	elapsed += dTime;
	if (elapsed > 0.25f) {
		pips++;
		elapsed = 0;
	}
	if (pips > 10)
		pips = 0;
	wstringstream ss;
	ss << L"Loading meshes(" << (int)(((float)mLoadData.loadedSoFar / (float)mLoadData.totalToLoad)*100.f) << L"%) ";
	for (int i = 0; i < pips; ++i)
		ss << L'.';
	pFont->DrawString(pFontBatch, ss.str().c_str(), Vector2(100, 200), Colours::White, 0, Vector2(0, 0), Vector2(1.f, 1.f));


	ss.str(L"");
	ss << L"FPS:" << (int)(1.f / dTime);
	pFont->DrawString(pFontBatch, ss.str().c_str(), Vector2(10, 550), Colours::White, 0, Vector2(0, 0), Vector2(0.5f, 0.5f));


	pFontBatch->End();


	d3d.EndRender();

}

/// <summary>
///  Handles all incoming inputs from windows messeges
/// </summary>
/// <param name="hwnd"></param>
/// <param name="msg"></param>
/// <param name="wParam"></param>
/// <param name="lParam"></param>
/// <returns></returns>
LRESULT Game::WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	const float camInc = 200.f * GetElapsedSec();
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		case 'a':
			PlayerPos.x -= 0.5f;
			PlayerPos.x = std::clamp(PlayerPos.x, -5.5f, 5.5f);
			break;
		case 'z':
			mCamPos.y -= camInc;
			break;
		case 'd':
			PlayerPos.x += 0.5f;
			PlayerPos.x  = std::clamp(PlayerPos.x, -5.5f, 5.5f);
			break;
		case 'f':
			mCamPos.x += camInc;
			break;
		case 'w':
			 
			break;
		case 's':
			mCamPos.z -= camInc;
			break;
		case 'v':
			LaunchBall = true;
			break;
		case ' ':
			mCamPos = mDefCamPos;
			break;
		}
	}
	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}
