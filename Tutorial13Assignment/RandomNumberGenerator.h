#pragma once


class RandonNumberGenerator {

	public:
		inline RandonNumberGenerator();
		inline ~RandonNumberGenerator();

		inline float getRandomValue(float max, float min) const;

private:
	inline void seed() const;
};

RandonNumberGenerator::RandonNumberGenerator() {
	seed();
//	cout << "\nCalling RandomNumberGenerator() - Default Constructor...";
}

RandonNumberGenerator::~RandonNumberGenerator() {
	//cout << "\nCalling ~RandomNumberGenerator() - destructor...";
}

float RandonNumberGenerator::getRandomValue(float max, float min) const {
	float temp = (float)rand() / RAND_MAX;
	temp = min + temp * (max - min);
	return temp;
}
void RandonNumberGenerator::seed() const {
	srand(static_cast<unsigned>(time(0)));
}
